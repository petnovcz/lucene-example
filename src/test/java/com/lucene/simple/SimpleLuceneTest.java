package com.lucene.simple;

import com.google.inject.internal.cglib.core.$KeyFactory;
import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.cz.CzechStemFilterFactory;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Trivial tests for indexing and search in Lucene
 */
public class SimpleLuceneTest
{
  @Test
  public void testLuceneWrite() throws Exception {

    // create index on disk
    Path path = Paths.get("target/simple-idx");
    Directory directory = FSDirectory.open(path);

    // prepare CZ search
    Analyzer customAnalyzer = CustomAnalyzer.builder()
            .withTokenizer("standard")
            .addTokenFilter("lowercase")
            .addTokenFilter(CzechStemFilterFactory.class)
            .addTokenFilter("asciifolding")
            .build();
    Map<String, Analyzer> perFieldAnalyzers = new HashMap<>();
    perFieldAnalyzers.put("title", customAnalyzer);
    Analyzer defaultAnalyzer = new StandardAnalyzer();
    Analyzer analyzer = new PerFieldAnalyzerWrapper(defaultAnalyzer, perFieldAnalyzers);

    // write into lucene index
    IndexWriter indexWriter = new IndexWriter(directory, new IndexWriterConfig(analyzer));
    Document doc1 = new Document();
    Document doc2 = new Document();

    // index documents to Apache Lucene
    doc1.add(new TextField("title", "Apple iPhone červená", Field.Store.YES));
    doc1.add(new TextField("category", "Apple iPhone", Field.Store.YES));

    // index documents to Apache Lucene
    doc2.add(new TextField("title", "Apple Watch červený", Field.Store.YES));
    doc2.add(new TextField("category", "Apple Watch", Field.Store.YES));

    indexWriter.addDocument(doc1);
    indexWriter.addDocument(doc2);
    indexWriter.commit();
    indexWriter.close();

    System.out.println("Documents indexed!");
  }

  @Test
  public void testLuceneSearch() throws Exception
  {
    // prepare field search analyzer
    Analyzer customAnalyzer = CustomAnalyzer.builder()
            .withTokenizer("standard")
            .addTokenFilter("lowercase")
            .addTokenFilter("synonymgraph", "synonyms", "synonyms.txt")
            .addTokenFilter(CzechStemFilterFactory.class)
            .addTokenFilter("asciifolding")
            .build();

    Map<String, Analyzer> perFieldAnalyzers = new HashMap<>();
    perFieldAnalyzers.put("title", customAnalyzer);
    perFieldAnalyzers.put("title", customAnalyzer);

    Analyzer defaultAnalyzer = new StandardAnalyzer();
    Analyzer analyzer = new PerFieldAnalyzerWrapper(defaultAnalyzer, perFieldAnalyzers);

    Path path = Paths.get("target/simple-idx");
    Directory directory = FSDirectory.open(path);

    // search docs
    QueryParser qp = new QueryParser("title", analyzer);
    Query idQuery = qp.parse("title:(+apple -iphone)");

    IndexReader reader = DirectoryReader.open(directory);
    IndexSearcher searcher = new IndexSearcher(reader);
    TopDocs hits = searcher.search(idQuery, 10);

    for (ScoreDoc sd : hits.scoreDocs) {
      Document d = searcher.doc(sd.doc);
      System.out.print(String.format(d.get("title")));
      System.out.println(" - " + sd);
    }

  }

  @Test
  public void testLuceneSimpleRead() throws Exception {
    Path path = Paths.get("target/simple-idx");
    Directory directory = FSDirectory.open(path);

    IndexReader reader = DirectoryReader.open(directory);
    Document d = reader.document(0);

    System.out.println(d);
  }
}